package Test;


import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.junit.Assert;
import static org.junit.Assert;

import java.util.concurrent.TimeUnit;

public class TestTask {

    public static void main (String args[]) throws UnirestException {

        HttpResponse<Item> itemResponse = Unirest.get("https://api.weather.yandex.ru/v1/forecast?")
                .queryString("lat", "55.833333")
                .queryString("lon", "37.616667")
                .queryString("extra", "true")
                .asObject(Item[].class);;


        Assert.assertEquals(itemResponse.lat, 515406);
        Assert.assertEquals(itemResponse.lon, 46.0086);
        Assert.assertEquals(itemResponse.offset, 10800);

        Assert.assertEquals(itemResponse.name, "Europe/Moscow");
        Assert.assertEquals(itemResponse.dst, false);

        Assert.assertEquals(itemResponse.url, "https://yandex.ru/pogoda/moscow");
        Assert.assertEquals(itemResponse.season, "summer");



    }

}
